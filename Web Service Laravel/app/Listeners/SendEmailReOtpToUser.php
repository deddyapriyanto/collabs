<?php

namespace App\Listeners;

use App\Mail\RegenerateTokenMail;
use App\Events\RegenerateOtpEvent;

use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailReOtpToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateOtpEvent  $event
     * @return void
     */
    public function handle(RegenerateOtpEvent $event)
    {
        //
        Mail::to($event->otpcode->user->email)->send(new RegenerateTokenMail($event->otpcode));
    }
}
