<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class ReKomentar extends Model
{
    //
    protected $table='re_komentars';
    protected $primaryKey='id';
    protected $keyType='string';
    public $incrementing=false;
    
    
    protected $fillable = [
         'user_id','komentar_id','re_komentar'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
          if (empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()}= Str::uuid();
          }
        });
        
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function komentar()
    {
        return $this->belongsTo('App\Komentar');
    }
}
