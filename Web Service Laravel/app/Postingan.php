<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    //
    protected $table='postingans';
    protected $primaryKey='id';
    protected $keyType='string';
    public $incrementing=false;
    
    protected $fillable = [
        'caption', 'gambar', 'tulisan','quote','user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
          if (empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()}= Str::uuid();
          }
          $model->user_id = auth()->user()->id;

        });
        
    }

    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    public function postingan_like(){
        return $this->hasMany('App\PostinganLike');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
