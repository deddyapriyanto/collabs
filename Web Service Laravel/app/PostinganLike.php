<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class PostinganLike extends Model
{
    //
    protected $table='postingan_likes';
    protected $primaryKey='id';
    protected $keyType='string';
    public $incrementing=false;
    
    protected $fillable = [
         'user_id', 'postingan_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
          if (empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()}= Str::uuid();
          }
          $model->user_id = auth()->user()->id;

        });
        
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postingan()
    {
        return $this->belongsTo('App\Postingan');
    }

}
