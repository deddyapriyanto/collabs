<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class FollowerUser extends Model
{
    //
    protected $table='follower_users';
    protected $primaryKey='id';
    protected $keyType='string';
    public $incrementing=false;
    
    protected $fillable = [
        'user_id', 'follower_id'
    ];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($model){
          if (empty($model->{$model->getKeyName()})){
            $model->{$model->getKeyName()}= Str::uuid();
          }
          $model->user_id = auth()->user()->id;

        });
        
    }
}
