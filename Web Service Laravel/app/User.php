<?php

namespace App;

use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username' , 'password' , 'email_verified_at'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            //$model->role_id = Role::where('name' , 'author')->first()->id;
        });
    }

    public function followings()
    {
        return $this->belongsToMany(User::class, 'follower_users', 'follower_id', 'user_id');
    }

    public function followers()
    {
        return $this->belongsToMany(User::class, 'follower_users', 'user_id', 'follower_id');
    }
    
    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    public function komentar_like(){
        return $this->hasMany('App\KomentarLike');
    }

    public function postingan_like(){
        return $this->hasMany('App\PostinganLike');
    }

    public function re_komentar(){
        return $this->hasMany('App\ReKomentar');
    }
    public function profile(){
        return $this->hasOne('App\Profile');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


}
