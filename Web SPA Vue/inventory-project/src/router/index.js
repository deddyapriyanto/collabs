import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/pegawai',
    name: 'Pegawai',
    component: () => import(/* webpackChunkName: "pegawai" */ '../views/Pegawai.vue')
  },
  {
    path: '/barangs',
    name: 'Barangs',
    component: () => import(/* webpackChunkName: "barang" */ '../views/Barang.vue')
  },
  {
    path: '/transaksi',
    name: 'Transaksi',
    component: () => import(/* webpackChunkName: "transaksi" */ '../views/Transaksi.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
